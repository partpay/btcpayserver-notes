#!/usr/bin/env python

# https://chromedriver.chromium.org/downloads

import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By


driver = webdriver.Chrome('/opt/chromedriver')
driver.get('http://127.0.0.1:4001')
driver.find_element_by_id('language-continue').click()
WebDriverWait(driver, 5).until(
    expected_conditions.text_to_be_present_in_element(
        (By.XPATH, '//h1'), 'Welcome')
)
title_field = driver.find_element_by_id("weblog_title")
title_field.clear()
title_field.send_keys("Automated Demo Site")

user_login_field = driver.find_element_by_id("user_login")
user_login_field.clear()
user_login_field.send_keys("test")


WebDriverWait(driver, 5).until(
    expected_conditions.element_to_be_clickable(
        (By.ID, 'pass1-text'))
)
pass1_field = driver.find_element_by_id('pass1-text')
pass1_field.click()
pass1_field.clear()
pass1_field.send_keys("12643uijf83,lJJ")

admin_email_field = driver.find_element_by_id("admin_email")
admin_email_field.clear()
admin_email_field.send_keys("test@test.test")

driver.find_element_by_id('submit').click()
time.sleep(0.5)

driver.get('http://127.0.0.1:4001/wp-login.php')

user_login_field = driver.find_element_by_id("user_login")
user_login_field.clear()
user_login_field.send_keys("test")

pass1_field = driver.find_element_by_id("user_pass")
pass1_field.clear()
pass1_field.send_keys("12643uijf83,lJJ")

driver.find_element_by_id('wp-submit').click();


driver.get('http://127.0.0.1:4001/wp-admin/plugin-install.php');

time.sleep(50)


driver.quit()
