#!/usr/bin/env python

# $ sudo pip install selenium
# Install driver: https://chromedriver.chromium.org/downloads
#
# sudo docker-compose up -d
# python3 selenium_btcpay.py
#
#
# Reset:
#  $ sudo docker-compose stop
#  $ sudo rm -r /tmp/part_docker/

import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By


def initialise_wallets():
    import subprocess
    template = 'sudo docker exec -t btcpayserver_particld{} particl-cli -datadir=/data {}'

    r = subprocess.call(template.format(1, 'extkeygenesisimport "abandon baby cabbage dad eager fabric gadget habit ice kangaroo lab absorb"'), shell=True)
    r = subprocess.call(template.format(1, 'walletsettings stakingoptions "{\"stakecombinethreshold\":\"100\",\"stakesplitthreshold\":200}"'), shell=True)

    r = subprocess.call(template.format(2, 'extkeyimportmaster "pact mammal barrel matrix local final lecture chunk wasp survey bid various book strong spread fall ozone daring like topple door fatigue limb olympic" "" true'), shell=True)
    r = subprocess.call(template.format(2, 'reservebalance true 1000000'), shell=True)
    r = subprocess.call(template.format(2, 'getnewextaddress lblExtTest'), shell=True)
    r = subprocess.call(template.format(2, 'rescanblockchain'), shell=True)
    r = subprocess.call(template.format(2, 'getwalletinfo'), shell=True)
    print('r', r)

    r = subprocess.call(template.format(3, 'extkeyimportmaster "cappero malafede sierra slitta vantaggio stima anacardo variante stampato ritegno istituto gonfio"'), shell=True)
    r = subprocess.call(template.format(3, 'reservebalance true 1000000'), shell=True)

    r = subprocess.call(template.format(3, 'extkey account default true'), shell=True)

def initialise_user(driver):

    driver.get('http://127.0.0.1:4000/Account/Register');

    email_field = driver.find_element_by_id('Email')
    email_field.clear()
    email_field.send_keys('test@test.test')

    password_field = driver.find_element_by_id('Password')
    password_field.clear()
    password_field.send_keys('jf83,lJJ12643ui')

    confirm_field = driver.find_element_by_id('ConfirmPassword')
    confirm_field.clear()
    confirm_field.send_keys('jf83,lJJ12643ui')

    driver.find_element_by_id('RegisterButton').click()


def initialise_store(driver, store_name):
    driver.get('http://127.0.0.1:4000/stores/create')

    store_name_field = driver.find_element_by_id('Name')
    store_name_field.clear()
    store_name_field.send_keys(store_name)

    driver.find_element_by_id('Create').click()


    driver.find_element_by_id('ModifyPART').click()

    xpub_field = driver.find_element_by_id('DerivationScheme')
    xpub_field.clear()
    xpub_field.send_keys('pparszKkC7oHxNvmsBR6FArRDgDupbhk4EpchFr3do3QQhznxt8KSR9CFfgAThEmy9PeJ82shuUfQ62c2pJz9JjZ1NCgU9VyCiiZ2v65thdTvnCf-[legacy]')

    driver.find_element_by_id('Continue').click()
    driver.find_element_by_id('Confirm').click()

    url = driver.current_url

    store_id = url.split('/')[-1]
    return store_id


def get_token(driver, store_id):
    driver.get('http://127.0.0.1:4000/stores/{}/Tokens/Create'.format(store_id))

    label_field = driver.find_element_by_id('Label')
    label_field.clear()
    label_field.send_keys('DemoToken')

    driver.find_element_by_id('RequestPairing').click()

    driver.find_element_by_id('ApprovePairing').click()

    url = driver.current_url
    print(url)

    token_id = url.split('=')[-1]
    print(token_id)
    return token_id

def get_api_key(driver, store_id):
    driver.get('http://127.0.0.1:4000/stores/{}/Tokens'.format(store_id))

    driver.find_element_by_xpath("//button[contains(.,'Create new API Key')]").click()
    time.sleep(1)
    api_key = driver.find_element_by_id('ApiKey').get_attribute('value')

    api_code = driver.find_element_by_xpath("//code").text

    print(api_key)
    print(api_code)

    return api_key, api_code


def main():

    initialise_wallets()

    store_name = 'DemoStore'

    driver = webdriver.Chrome('/opt/chromedriver')
    initialise_user(driver)
    store_id = initialise_store(driver, store_name)

    token_id = get_token(driver, store_id)

    api_key, api_code = get_api_key(driver, store_id)

    time.sleep(1)
    driver.quit()

    print('store_id', store_id)
    print('token_id', token_id)

    from btcpay import BTCPayClient
    import pickle
    import base64
    import json
    import requests

    client = BTCPayClient.create_client(host='http://127.0.0.1:4000', code=token_id)

    print(base64.b64encode(pickle.dumps(client)))

    time.sleep(5) # testing where to delay
    # try avoid 40 seconds of create_invoice failed ('Connection broken: IncompleteRead(0 bytes read)', IncompleteRead(0 bytes read))

    for i in range(0, 60):
        try:
            new_invoice = client.create_invoice({"price": 1.0, "currency": "PART"})
            #new_invoice = client.create_invoice({"price": 1.0, "currency": "USD"})
            print(json.dumps(new_invoice, indent=4))
            break
        except Exception as e:
            print('create_invoice failed', e)
            time.sleep(1)

    start_time = time.time()

    completed_tokens = 0
    for i in range(0, 100):
        try:
            new_invoice = client.create_invoice({"price": 1.0, "currency": "PART"})
            #new_invoice = client.create_invoice({"price": 1.0, "currency": "USD"})
            print(json.dumps(new_invoice, indent=4))
            completed_tokens += 1
        except Exception as e:
            print('create_invoice failed', e)
            time.sleep(1)

    time_tokens = time.time() - start_time
    print('Created {} requests in {} seconds'.format(completed_tokens, time_tokens))


    url = 'http://127.0.0.1:4000/invoices'
    body = {'price': 1.0, 'currency': 'USD'}
    headers = {'content-type': 'application/json',
            'Authorization': api_code.split(':')[1].strip()}

    r = requests.post(url, data=json.dumps(body), headers=headers)
    print('r', r)
    print('r.content', json.dumps(json.loads(r.content), indent=4))

    start_time = time.time()
    completed_basic = 0
    for i in range(0, 100):
        try:
            r = requests.post(url, data=json.dumps(body), headers=headers)
            print('r.content', json.dumps(json.loads(r.content), indent=4))
            completed_basic += 1
        except Exception as e:
            print('create_invoice failed', e)
            time.sleep(1)

    time_basic = time.time() - start_time
    print('Created {} token requests in {} seconds'.format(completed_tokens, time_tokens))
    print('Created {} basic requests in {} seconds'.format(completed_basic, time_basic))

    print('Done.')


if __name__ == '__main__':
    main()
